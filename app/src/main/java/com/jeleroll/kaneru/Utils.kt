package com.jeleroll.kaneru

import android.content.Context
import android.webkit.JavascriptInterface

private const val GOLDEN_TABLE = "com.GOLDEN.table.121"
private const val GOLDEN_ARGS = "com.GOLDEN.value.123"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(GOLDEN_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(GOLDEN_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(GOLDEN_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(GOLDEN_ARGS, null)
}
